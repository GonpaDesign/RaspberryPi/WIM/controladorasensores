#include <stdio.h> 		/* printf, sprintf */
#include <stdlib.h> 	/* exit, atoi, malloc, free */
#include <unistd.h> 	/* read, write, close */
#include <string.h> 	/* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> 		/* struct hostent, gethostbyname */
#include <wiringPi.h> 	/* access to GPIO of Raspberry Pi */
#include <time.h> 		/* timeinfo */
#include <exception>
#include <thread>
#include <iostream>
#include <fstream>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.

using namespace std;

#define eventLogFile "/home/pi/rpi_wim/WIM_Sensores2/log/event.log"
#define sensorLogFile "/home/pi/rpi_wim/WIM_Sensores2/log/sensor.log"
#define configFile "/home/pi/rpi_wim/WIM_Sensores2/config.json"

int inform = 0x00;
int DEBUG = 1;
char ip[15];
int port = 8081;
int ID = 0;
unsigned int timeout = 0;

typedef struct
{
	int pin = 0;
	unsigned int bounce = 50;
	unsigned int hold = 0;
	int state = 0;
	int time = 0;
	int stateTime = 0;
	bool updated = 0;
	int na = 0;
} sensor;

typedef struct
{
	int singleState = 0;
	int dualState = 0;
	unsigned int time = 0;
} dual;

sensor 	optic;
sensor 	inShaft;
sensor 	outShaft;
sensor 	singleShaft;
sensor 	doubleShaft;
sensor 	height;
dual	shaftType;

void log(int type, char* description)

{
	time_t rawTime;
	struct tm * timeinfo;
	time(&rawTime);
	timeinfo = localtime (&rawTime);
	FILE *f = fopen(eventLogFile, "a");
	if(f == NULL)
	{
		printf("Error opening file");
	}
	char Date[20];
	sprintf(Date, "%02d/%02d/%d  %02d:%02d:%02d", timeinfo->tm_mday, timeinfo->tm_mon + 1,timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	switch(type)
	{
		case 0:
			fprintf(f, "%s	%s	%s\n", Date, "DEBUG", description);
		break;
		case 1:
			fprintf(f, "%s	%s	%s\n", Date, "INFO", description);
		break;
		case 2:
			fprintf(f, "%s	%s	%s\n", Date, "ERROR", description);
		break;
		case 3:
			fprintf(f, "%s	%s	%s\n", Date, "TRACE", description);
		break;
	}
	fclose(f);
}

void logSensor(int type, char* description)
{
	time_t rawTime;
	struct tm * timeinfo;
	time(&rawTime);
	timeinfo = localtime (&rawTime);
	FILE *f = fopen(sensorLogFile, "a");
	if(f == NULL)
	{
		printf("Error opening file");
	}
	char Date[20];
	sprintf(Date, "%02d/%02d/%d  %02d:%02d:%02d", timeinfo->tm_mday, timeinfo->tm_mon + 1,timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	switch(type)
	{
		case 0:
			fprintf(f, "%s	%s	%s\n", Date, "DEBUG", description);
		break;
		case 1:
			fprintf(f, "%s	%s	%s\n", Date, "INFO", description);
		break;
		case 2:
			fprintf(f, "%s	%s	%s\n", Date, "ERROR", description);
		break;
		case 3:
			fprintf(f, "%s	%s	%s\n", Date, "TRACE", description);
		break;
	}
	fclose(f);
}

void error(const char *msg) 
{ 
	perror(msg);
	char emsg[] = "Unable to update states";
	log(2, emsg);
}
 
int httpGET(int _height, int _optic, int _in, int _out, int _single, int _double)
{
	int msg_size = strlen("Informando: Id Equipo: d - Altura: d - EnTransito: d - Eje Entrada: d - Eje Simple: d - Eje Dual: d - Eje Salida: d");
	char msg[msg_size + 1]; 
	sprintf(msg, "Informando: Id Equipo: %d - Altura: %d - EnTransito: %d - Eje Entrada: %d - Eje Simple: %d - Eje Dual: %d - Eje Salida: %d", ID, _height, _optic, _in, _single, _double, _out);
	logSensor(3, msg);
	struct hostent *server;
	struct sockaddr_in serv_addr;
	int sockfd, bytes, sent, received, total;
	char response[4096];
	char message[512];
	sprintf(message, "GET /lecturas/setLecturaSensores?altura=%d&en_transito=%d&entrada_eje=%d&eje_simple=%d&eje_doble=%d&salida_eje=%d&dispositivo=%d HTTP/1.1\r\nUser-Agent: Mozilla/5.0\r\nHost: %s:%d\r\nConnection: close\r\n\r\n", _height, _optic, _in, _single, _double, _out, ID, ip, port);
	// What are we going to send? 
	if(DEBUG)
	{
		printf("Request:\n%s\n",message);
	}
	// create the socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
	{
		error("ERROR opening socket");
		return 0;
	}
	// lookup the ip address
	server = gethostbyname(ip);
	if (server == NULL) 
	{
		error("ERROR, no such host");
		return 0;
	}	
	// fill in the structure
	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);
	// connect the socket
	if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
	{
		error("ERROR connecting");
        return 0;
	}
	// send the request
	total = strlen(message);
	sent = 0;
	do {
		bytes = write(sockfd,message+sent,total-sent);
		if (bytes < 0)
			error("ERROR writing message to socket");
		if (bytes == 0)
			break;
		sent+=bytes;
	} while (sent < total);
	// receive the response
	memset(response,0,sizeof(response));
	total = sizeof(response)-1;
	received = 0;
	do {
		bytes = read(sockfd,response+received,total-received);
		if (bytes < 0)
			error("ERROR reading response from socket");
		if (bytes == 0)
			break;
		received+=bytes;
	} while (received < total);
	if (received == total)
	{
		error("ERROR storing complete response from socket");
        return 0;
	}
	// close the socket 
	close(sockfd);
	// process response 
	//printf("%s\n",response);
	char *pend = strrchr(response, '{');
	int pendLen = strlen(pend);
	pend[pendLen - 4] = 0;
	log(3,pend);
    return 1;
}

void informStateChange(int senState)
{
	int change = 0x01;
	int heightSen = 0, opticSen = 0, inSen = 0, outSen = 0, singleSen = 0, doubleSen = 0;
	change &= senState;
	if(change != 0)
	{
		printf("Updating height sensor\n");
		heightSen = height.state;
		height.updated = true;
	}
	change = senState & 0x02;
	if(change != 0)
	{
		printf("Updating optic sensor\n");
		opticSen = optic.state;
		optic.updated = true;
	}
	change = senState & 0x04;
	if(change != 0)
	{
		printf("Updating inShaft sensor\n");
		inSen = inShaft.state;
		inShaft.updated = true;
	}
	change = senState & 0x08;
	if(change != 0)
	{
		printf("Updating outShaft sensor\n");
		outSen = outShaft.state;
		outShaft.updated = true;
	}
	change = senState & 0x10;
	if(change != 0)
	{
		printf("Updating singleShaft sensor and dualShaft sensor\n");
		printf("Updating singleShaft sensor\n");
		singleSen = singleShaft.state;
		doubleSen = doubleShaft.state;
		singleShaft.updated = true;
		doubleShaft.updated = true;
		printf("singleShaft value: %d dualShaft: %d\n", singleSen, doubleSen);
		printf("singleShaft value: %d\n", singleSen);
	}
	if(!httpGET(heightSen, opticSen, inSen, outSen, singleSen, doubleSen))
	{
		char msg[] = "Unable to inform states";
		log(2, msg);
	}
}

int readConfiguration()
{
	try
	{
		ifstream systemConfig(configFile);
		Json::Reader reader;
		Json::Value jsonObj;
		reader.parse(systemConfig, jsonObj); 

		// Carga de configuracion del sistema
		const Json::Value& system = jsonObj["system"]; // array of characters
		ID = atoi(system["id"].asString().c_str());
		DEBUG = atoi(system["debug"].asString().c_str());;

		// Carga de configuracion del Web Client
		const Json::Value& wc = jsonObj["webClient"];
		strcpy(ip,strdup(wc["IP"].asString().c_str()));
		port = atoi(wc["port"].asString().c_str());
		timeout = stoul(wc["timeOut"].asString().c_str());
		
		// Carga de configuracion del Sensor optico
		const Json::Value& opticObj = jsonObj["optic_sensor"]; // array of characters
		optic.pin = atoi(opticObj["pin"].asString().c_str());
		optic.bounce = stoul(opticObj["bounce"].asString().c_str());
		optic.na = atoi(opticObj["na"].asString().c_str());
		optic.hold = stoul(opticObj["hold"].asString().c_str());

		// Carga de configuracion del Sensor Altura
		const Json::Value& heightObj = jsonObj["height_sensor"]; // array of characters
		height.pin = atoi(heightObj["pin"].asString().c_str());
		height.bounce = stoul(heightObj["bounce"].asString().c_str());
		height.na = atoi(heightObj["na"].asString().c_str());
		height.hold = stoul(heightObj["hold"].asString().c_str());

		// Carga de configuracion del Sensor de Entrada de Ejes
		const Json::Value& inShaftObj = jsonObj["optic_sensor"]; // array of characters
		inShaft.pin = atoi(inShaftObj["pin"].asString().c_str());
		inShaft.bounce = stoul(inShaftObj["bounce"].asString().c_str());
		inShaft.na = atoi(inShaftObj["na"].asString().c_str());
		inShaft.hold = stoul(inShaftObj["hold"].asString().c_str());

		// Carga de configuracion del Sensor de Salida de Ejes
		const Json::Value& outShaftObj = jsonObj["optic_sensor"]; // array of characters
		outShaft.pin = atoi(outShaftObj["pin"].asString().c_str());
		outShaft.bounce = stoul(outShaftObj["bounce"].asString().c_str());
		outShaft.na = atoi(outShaftObj["na"].asString().c_str());
		outShaft.hold = stoul(outShaftObj["hold"].asString().c_str());

		// Carga de configuracion del Sensor de Ejes Simples
		const Json::Value& singleShaftObj = jsonObj["optic_sensor"]; // array of characters
		singleShaft.pin = atoi(singleShaftObj["pin"].asString().c_str());
		singleShaft.bounce = stoul(singleShaftObj["bounce"].asString().c_str());
		singleShaft.na = atoi(singleShaftObj["na"].asString().c_str());
		singleShaft.hold = stoul(singleShaftObj["hold"].asString().c_str());

		// Carga de configuracion del Sensor de Ejes Duales
		const Json::Value& doubleShaftObj = jsonObj["optic_sensor"]; // array of characters
		doubleShaft.pin = atoi(doubleShaftObj["pin"].asString().c_str());
		doubleShaft.bounce = stoul(doubleShaftObj["bounce"].asString().c_str());
		doubleShaft.na = atoi(doubleShaftObj["na"].asString().c_str());
		doubleShaft.hold = stoul(doubleShaftObj["hold"].asString().c_str());
		
		if(DEBUG == 1)
		{
			printf("--- System Configuration ---\n");
			printf("--- System ---");
			printf("%s %d\n", "Product Id: ", ID);
			printf("%s %s\n", "System debug: ", DEBUG == 1 ? "True" : "False");
			printf("--- WebClient ---");
			printf("%s %s\n", "host / ip: ", ip);
			printf("%s %d\n", "port: ", port);
			printf("%s %d\n", "timeOut: ", timeout);
			printf("--- Optic Sensor ---\n");
			printf("%s %d\n", "I/O Pin: ", optic.pin);
			printf("%s %d %s\n", "Bounce time: ", optic.bounce, "ms.");
			printf("%s %s\n", "Normal Open logic: ", optic.na == 1 ? "True" : "False"); 
			printf("%s %ud %s\n", "Hold state change: ", optic.hold, "ms.");
			printf("--- Height Sensor ---\n");
			printf("%s %d\n", "I/O Pin: ", height.pin);
			printf("%s %d %s\n", "Bounce time: ", height.bounce, "ms.");
			printf("%s %s\n", "Normal Open logic: ", height.na == 1 ? "True" : "False"); 
			printf("%s %ud %s\n", "Hold state change: ", height.hold, "ms.");
			printf("--- Incomming Shaft Sensor ---\n");
			printf("%s %d\n", "I/O Pin: ", inShaft.pin);
			printf("%s %d %s\n", "Bounce time: ", inShaft.bounce, "ms.");
			printf("%s %s\n", "Normal Open logic: ", inShaft.na == 1 ? "True" : "False"); 
			printf("%s %ud %s\n", "Hold state change: ", inShaft.hold, "ms.");
			printf("--- Outcomming Shaft Sensor ---\n");
			printf("%s %d\n", "I/O Pin: ", outShaft.pin);
			printf("%s %d %s\n", "Bounce time: ", outShaft.bounce, "ms.");
			printf("%s %s\n", "Normal Open logic: ", outShaft.na == 1 ? "True" : "False"); 
			printf("%s %ud %s\n", "Hold state change: ", outShaft.hold, "ms.");
			printf("--- Single Shaft Sensor ---\n");
			printf("%s %d\n", "I/O Pin: ", singleShaft.pin);
			printf("%s %d %s\n", "Bounce time: ", singleShaft.bounce, "ms.");
			printf("%s %s\n", "Normal Open logic: ", singleShaft.na == 1 ? "True" : "False"); 
			printf("%s %ud %s\n", "Hold state change: ", singleShaft.hold, "ms.");
			printf("--- Dual Shaft Sensor ---\n");
			printf("%s %d\n", "I/O Pin: ", doubleShaft.pin);
			printf("%s %d %s\n", "Bounce time: ", doubleShaft.bounce, "ms.");
			printf("%s %s\n", "Normal Open logic: ", doubleShaft.na == 1 ? "True" : "False"); 
			printf("%s %ud %s\n", "Hold state change: ", doubleShaft.hold, "ms.");
		}
		return 1;
	}catch(exception& e){
		cout << "exception ocurr" << endl;
		return 0;
	}
}

void begin()
{
	// Declaracion de pines
	printf("%s\n", "defining I/O");
	
	pinMode(height.pin, INPUT);
	pinMode(optic.pin, INPUT);
	pinMode(inShaft.pin, INPUT);
	pinMode(outShaft.pin, INPUT);
	pinMode(singleShaft.pin, INPUT); 
	pinMode(doubleShaft.pin, INPUT);
	
	// Inicializacion de estados 
	printf("%s\n", "initializing I/O states");
	height.state = digitalRead(height.pin);
	height.state = millis();
	height.updated = false;
	optic.state = digitalRead(optic.pin);
	optic.time = millis();
	optic.updated = false;
	inShaft.state = digitalRead(inShaft.pin);
	inShaft.time = millis();
	optic.updated = false;
	outShaft.state = digitalRead(outShaft.pin);
	outShaft.time = millis();
	outShaft.updated = false;
	singleShaft.state = digitalRead(singleShaft.pin);
	singleShaft.time = millis();
	singleShaft.updated = false;
	doubleShaft.state = digitalRead(doubleShaft.pin);
	doubleShaft.time = millis();
	doubleShaft.updated = false;
}

int readSensor(sensor *sen, int value)
{
	if(sen->state != digitalRead(sen->pin))
	{
		sen->stateTime = millis() - sen->time;
		if(DEBUG == 2)
		{
			if((sen->na == 1 && sen->state == 0) || (sen->na == 0 && sen->state == 1))
			{
				printf("%s %d %s\n", "Sensor remained OFF: ", sen->stateTime, "milliseconds");
				printf("Sensor state changed. Current state: ON\n");
			}
			else
			{
				printf("%s %d %s\n", "Sensor remained ON: ", sen->stateTime, "milliseconds");
				printf("Sensor state changed. Current state: OFF\n");
			}
		}
		if((sen->state != sen->na) && (sen->hold == 0)){sen->updated = false;}
		sen->state = digitalRead(sen->pin);
		sen->time = millis();
	}
	else
	{
		if(millis() - sen->time >= sen->bounce)
		{
			if((sen->updated == false) && (sen->state == sen->na))
			{
				return value;
			}
		}
		if(sen->hold != 0){
			if((millis() - sen->time >= sen->hold) && (sen->updated == true) && (sen->state != sen->na))
			{
				sen->updated = false;
			}
		}
	}
	return 0x00;
}

int main()
{
	if(wiringPiSetup() < 0)
	{
		printf("%s\n", "wiringPi load error");
		char msg[] = "Unable to load wiringPi";
		log(2, msg);
		return 0;
	}

	if(readConfiguration() == 0)
	{
		char msg[] = "Unable to read config file";
		log(2, msg);
		return 0;
	}
	
	begin();
	
	for(;;)
	{	
		inform |= readSensor(&height, 0x01);
		inform |= readSensor(&optic, 0x02);
		if((optic.state == 1 && optic.na == 1) || (optic.state == 0 && optic.na == 0))
		{
			inform |= readSensor(&inShaft, 0x04);
		}
		inform |= readSensor(&outShaft, 0x08);
		readSensor(&doubleShaft, 0x20);
		inform |= readSensor(&singleShaft, 0x10);
		if(inform != 0x00)
		{
			printf("inform value: %d\n", inform);
			thread t1 (informStateChange, inform);
			t1.detach();
			inform = 0x00;
		}
		delay(2);
	}
	return 1;
}
